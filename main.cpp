#include <iostream>

typedef unsigned int uint;

using namespace std;

unsigned int minimum(unsigned int n) {
    n=(n*(n-1))/2;
    if(100000>n)
        return n;
    return 100000;
}

int main()
{
    uint mul = static_cast<uint>(10e+9);
    unsigned int n=1; //sumaryczna liczba dzieci
    unsigned int m=0; //sumaryczna liczba par dzieci
    unsigned short z = 1; //liczba zapytan
    unsigned int **pary; //np. pary[1][2], pary[0][1] -> tablica dwuwymiarowa
    unsigned int *kat; //np. kat[2], kat[1] -> tablica jednowymiarowa

    cin >> n >> m >> z;
    if (n>100000 || n<1 || z<1)
        return -1;
    if (m>0) {
        if (m>minimum(n))
            return -2;
        pary = new unsigned int*[m];
        for (unsigned int i=0;i<m;i++){
            pary[i] = new unsigned int[2];
            cin >> pary[i][0] >> pary[i][1];
        }
    }
    kat = new unsigned int[z];
    for(unsigned int i=0;i<z;i++) {
        cin >> kat[i];
        if (kat[i]>mul)
            return -5;
    }
    //obliczenia
    for (unsigned int i=0;i<z;i++) {
        if (m>0)
            n=m;
       // cout << kat[i]*n << " <-wynik " << i << '\n';
        kat[i]=kat[i]*n%mul+7;
    }
    //sprawdzenie wynikow
    for (unsigned int i=0;i<z;i++) {
        cout << kat[i];
    }
    return 0;
}
